Copyright © 2014-2018 S.Reissfelder [SR]

Ownership of copyright

The copyright in this website and the material on this website (including without limitation the text, computer code, artwork, photographs, images, music, audio material, video material and audio-visual material on this website) is owned by SR.

Copyright license

SR grants to you a worldwide non-exclusive royalty-free revocable license to:
- view this website and the material on this website on a computer or mobile device via a web browser;
- copy and store this website and the material on this website in your web browser cache memory; and
- print pages from this website for your own [personal and non-commercial] use.
SR does not grant you any other rights in relation to this website or the material on this website. In other words, all other rights are reserved.

For the avoidance of doubt, you must not adapt, edit, change, transform, publish, republish, distribute, redistribute, broadcast, rebroadcast or show or play in public this website or the material on this website (in any form or media) without SR’s prior written permission.

Data mining
- The automated and/or systematic collection of data from this website is prohibited.

Permissions
- You may request permission to use the copyright materials on this website by writing to the copyright holder.

Enforcement of copyright

SR takes the protection of its copyright very seriously. If SR discovers that you have used its copyright materials in contravention of the license above, SR may bring legal proceedings against you seeking monetary damages and an injunction to stop you using those materials. You could also be ordered to pay legal costs.

If you become aware of any use of SR’s copyright materials that contravenes or may contravene the license above, please report this by email to the copyright holder.