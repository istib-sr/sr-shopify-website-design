gulp = require('gulp')

coffee = require('gulp-coffee')
sass = require('gulp-sass')
cssimport = require("gulp-cssimport")

watch = require('gulp-watch')
concat = require('gulp-concat')
rename = require("gulp-rename")
open = require("gulp-open")

gulp.task 'clean', ->
# ----------------------------------

gulp.task 'coffee', ->
# ----------------------------------
  gulp.src('./scripts/src/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(rename({extname: ".js.liquid"}))
    .pipe(gulp.dest('shop/assets/'))

gulp.task 'js', ->
# ----------------------------------
  gulp.src('./scripts/src/*.js')
    .pipe(rename({extname: ".js.liquid"}))
    .pipe(gulp.dest('shop/assets/'))

gulp.task 'libraries', ->
# ----------------------------------
  gulp.src('./scripts/lib/*.*')
    .pipe(rename({extname: ".js.liquid"}))
    .pipe(gulp.dest('shop/assets/'))

gulp.task 'theme-styles', ->
# ----------------------------------
  gulp.src('./theme/[^_]*.*')
      .pipe(rename({extname: ".css.liquid"}))
      .pipe(gulp.dest('./shop/assets/'))

gulp.task 'styles', ->
# ----------------------------------
  gulp.src(['./styles/*.*'])
      # .pipe(sass())
      # .pipe(cssimport())
      .pipe(concat("main"))
      .pipe(rename({extname: ".scss.liquid"}))
      .pipe(gulp.dest('./shop/assets/'))

# ----------------------------------

gulp.task 'fonts', ->
  gulp.src(['./fonts/*.*'])
    .pipe(gulp.dest('shop/assets/'))

gulp.task 'images', ->
  gulp.src(['./images/*.*'])
    .pipe(gulp.dest('shop/assets/'))

gulp.task 'templates', ->
  gulp.src(['./templates/**/*.*'])
    .pipe(gulp.dest('shop/templates/'))

gulp.task 'layout', ->
  gulp.src(['./layout/**/*.*'])
    .pipe(gulp.dest('shop/layout/'))

gulp.task 'snippets', ->
  gulp.src(['./snippets/**/*.*'])
    .pipe(gulp.dest('shop/snippets/'))

gulp.task 'config', ->
  gulp.src(['./config/*.*'])
    .pipe(gulp.dest('shop/config/'))

gulp.task 'locales', ->
  gulp.src(['./locales/*.*'])
    .pipe(gulp.dest('shop/locales/'))

# gulp.task 'upload', ->
#   gulp.run 'grunt-shopify:upload'

# gulp.task 'open', ->
#   gulp.src('./store/layout/theme.liquid')
#     .pipe open "",
#       url: "http://localhost:3000"
#       app: 'firefox'

# ----------------------------------

gulp.task 'build', ->
# ----------------------------------
  gulp.start 'coffee'
  gulp.start 'js'
  gulp.start 'libraries'
  gulp.start 'theme-styles'
  gulp.start 'styles'
  gulp.start 'fonts'
  gulp.start 'images'
  gulp.start 'templates'
  gulp.start 'layout'
  gulp.start 'snippets'
  gulp.start 'config'
  gulp.start 'locales'

gulp.task 'default', ->
  gulp.start 'build'
  # gulp.start 'upload'

gulp.task 'watch', ->
# ----------------------------------
  watch ['./scripts/**/*.*',
         './styles/*.*',
         './templates/**/*.*',
         './snippets/*.*',
         './layout/*.*',
         './images/*.*',
         './config/*.*'], ->
    gulp.start('default')

